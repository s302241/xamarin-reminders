﻿using System;
using Newtonsoft.Json;
using SQLite;

namespace Reminders.Models
{
  public class Reminder
  {
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }

    [JsonProperty("text")]
    public string Text { get; set; }

    [JsonProperty("completed")]
    public bool Completed { get; set; }

    public bool IsNew => ID == 0;

    public Reminder()
    {
    }
  }
}
