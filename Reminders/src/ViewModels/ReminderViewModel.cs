﻿using System;
using System.Threading.Tasks;
using Foundation;
using Reminders.Models;
using Reminders.res;
using Reminders.Views;
using Xamarin.Forms;

namespace Reminders.ViewModels
{
  public class ReminderViewModel : ObservableObject
  {
    private readonly Reminder _reminder;

    private readonly Command _cancelCommand;
    private readonly Command _doneCommand;
    private readonly Command _deleteCommand;

    public event EventHandler Disappearing;

    public Command CancelCommand => _cancelCommand;
    public Command DoneCommand => _doneCommand;
    public Command DeleteCommand => _deleteCommand;

    public string Title
    {
      get
      {
        var result = string.Empty;
        if (_reminder != null)
        {
          if (_reminder.IsNew)
          {
            result = AppResources.ReminderPageAddReminder;
          }
          else
          {
            var text = _reminder.Text ?? string.Empty;
            var idx = text.IndexOf(' ');
            if (idx > 0)
            {
              result = text.Substring(0, idx);
            }
            else
            {
              result = text;
            }
          }
        }
        return result;
      }
    }

    public string Text
    {
      get => _reminder.Text;
      set
      {
        if (_reminder.Text == value) return;

        _reminder.Text = value;
        OnPropertyChanged();
      }
    }

    public bool Completed
    {
      get => _reminder.Completed;
      set
      {
        if (_reminder.Completed == value) return;

        _reminder.Completed = value;
        OnPropertyChanged();
      }
    }

    public ReminderViewModel(Reminder reminder)
    {
      _reminder = reminder;

      _cancelCommand = new Command(Cancel);
      _doneCommand = new Command(Done);
      _deleteCommand = new Command(Delete);
    }

    protected void OnDisappearing()
    {
      Disappearing?.Invoke(this, EventArgs.Empty);
    }

    private void Cancel(object parameter)
    {
      _ = CancelAsync();
    }

    private async Task CancelAsync()
    {
      OnDisappearing();

      await Application.Current.MainPage.Navigation.PopModalAsync();
    }

    private void Done(object parameter)
    {
      _ = DoneAsync();
    }

    private async Task DoneAsync()
    {
      await App.Database.SaveReminderAsync(_reminder);

      OnDisappearing();

      await Application.Current.MainPage.Navigation.PopModalAsync();
    }

    private void Delete(object paramter)
    {
      _ = DeleteAsync();
    }

    private async Task DeleteAsync()
    {
      await App.Database.DeleteReminderAsync(_reminder);

      OnDisappearing();

      await Application.Current.MainPage.Navigation.PopModalAsync();
    }
  }
}
