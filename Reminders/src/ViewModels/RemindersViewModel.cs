﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Foundation;
using Reminders.Models;
using Reminders.Views;
using Xamarin.Forms;

namespace Reminders.ViewModels
{
  public class RemindersViewModel : ObservableObject
  {
    private readonly Command _addReminderCommand;

    private readonly ObservableCollection<ReminderViewModel> _reminders
      = new ObservableCollection<ReminderViewModel>();

    private ReminderViewModel _selectedReminder;

    public Command AddReminderCommand => _addReminderCommand;

    public ObservableCollection<ReminderViewModel> Reminders => _reminders;

    public ReminderViewModel SelectedReminder
    {
      get => _selectedReminder;
      set
      {
        if (SetProperty(ref _selectedReminder, value)
          && _selectedReminder != null)
        {
          _ = GoToReminderPage(_selectedReminder);

          SelectedReminder = null;
        }
      }
    }

    public RemindersViewModel()
    {
      _addReminderCommand = new Command(AddReminder);

      _ = LoadRemindersAsync();
    }

    private void AddReminder(object parameter)
    {
      _ = AddReminderAsync();
    }

    private async Task AddReminderAsync()
    {
      var reminder = new Reminder();

      var reminderVM = new ReminderViewModel(reminder);
      reminderVM.Disappearing += ReminderVM_Disappearing;

      await GoToReminderPage(reminderVM);
    }

    private async Task GoToReminderPage(ReminderViewModel reminderViewModel)
    {
      var reminderPage = new ReminderPage();
      reminderPage.BindingContext = reminderViewModel;

      var navigationPage = new NavigationPage(reminderPage);

      await Application.Current.MainPage.Navigation.PushModalAsync(
        navigationPage);
    }

    private void ReminderVM_Disappearing(object sender, EventArgs e)
    {
      _ = LoadRemindersAsync();
    }

    private async Task LoadRemindersAsync()
    {
      _reminders.Clear();

      var reminders = await App.Database.GetRemindersAsync();

      foreach (var reminder in reminders)
      {
        var vm = new ReminderViewModel(reminder);
        vm.Disappearing += ReminderVM_Disappearing;
        _reminders.Add(vm);
      }
    }
  }
}
