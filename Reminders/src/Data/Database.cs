﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Reminders.Models;
using SQLite;

namespace Reminders.Data
{
  public class Database
  {
    private readonly SQLiteAsyncConnection _database;

    public Database(string dbPath)
    {
      _database = new SQLiteAsyncConnection(dbPath);

      Initialize();
    }

    public Task<List<Reminder>> GetRemindersAsync()
    {
      return _database.Table<Reminder>().ToListAsync();
    }

    public Task<Reminder> GetReminderAsync(int id)
    {
      var result = _database
        .Table<Reminder>()
        .Where(i => i.ID == id)
        .FirstOrDefaultAsync();
      return result;
    }

    public Task<int> SaveReminderAsync(Reminder reminder)
    {
      if (reminder.ID != 0)
      {
        return _database.UpdateAsync(reminder);
      }
      else
      {
        return _database.InsertAsync(reminder);
      }
    }

    public Task<int> DeleteReminderAsync(Reminder reminder)
    {
      return _database.DeleteAsync(reminder);
    }

    private void Initialize()
    {
      _database.CreateTableAsync<Reminder>().Wait();
    }
  }
}
