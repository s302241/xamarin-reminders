﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Reminders.Views;
using Reminders.Data;
using System.IO;

namespace Reminders
{
  public partial class App : Application
  {
    private static Database _database;

    public static Database Database
    {
      get
      {
        if (_database == null)
        {
          _database = new Database(Path.Combine(Environment.GetFolderPath(
            Environment.SpecialFolder.LocalApplicationData),
            "database.db3"));
        }
        return _database;
      }
    }

    public App()
    {
      InitializeComponent();

      var remaindersPage = new RemindersPage();
      var navigationPage = new NavigationPage(remaindersPage);

      MainPage = navigationPage;
    }

    protected override void OnStart()
    {
    }

    protected override void OnSleep()
    {
    }

    protected override void OnResume()
    {
    }
  }
}
